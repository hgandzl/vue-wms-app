import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isCollapse: false,
    asideWidth: "200px",
    iconDirection: "el-icon-s-fold",
    userInfo: {
      no: "",
      name: "",
      token: "",
    },
  },
  getters: {
    userInfo: state => state.userInfo
  },
  mutations: {
    collapseMenu(state) {
      state.isCollapse = !state.isCollapse;
      if (state.isCollapse) {
        state.asideWidth = "64px";
        state.iconDirection = "el-icon-s-unfold";
      } else {
        state.asideWidth = "200px";
        state.iconDirection = "el-icon-s-fold";
      }
    },
    setUser(state, payload) {
      state.userInfo = payload;
    },
    clearUser(state) {
      state.userInfo = {
        no: "",
        name: "",
        token: "",
      };
    },
  },
  actions: {},
  modules: {},
});
