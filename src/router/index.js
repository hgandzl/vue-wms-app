import Vue from "vue";
import VueRouter from "vue-router";
import Main from "../views/Main.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: "/home",
    component: Main,
    children: [
      {
        path: "/home",
        name: "home",
        component: () => import("../views/Home.vue"),
      },
      {
        path: "/userinfo",
        component: () => import("../views/UserInfo.vue"),
      },
      {
        path: "/admin",
        name: "admin",
        component: () => import("../views/AdminManage.vue"),
      },
      {
        path: "/user",
        name: "user",
        component: () => import("../views/UserManage.vue"),
      },
    ],
  },
  // {
  //   path: "/home",
  //   name: "home",
  //   component: () => import("../components/Index.vue"),
  //   // chirdren:[
  //   //   {
  //   //     path: '/userinfo',
  //   //     name: 'userinfo',
  //   //     meta: '首页'
  //   //     // component
  //   //   }
  //   // ]
  // },
  {
    path: "/login",
    name: "login",
    component: () => import("../views/Login.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

// 路由守卫
import jwt_decode from "jwt-decode";
router.beforeEach((to, from, next) => {
  const isLogin = localStorage.wmstoken ? true : false;
  if (isLogin) {
    const decode = jwt_decode(localStorage.wmstoken);
    const date = parseInt(new Date().getTime() / 1000);
    if (date - decode.iat > decode.exp - decode.iat) {
      localStorage.removeItem("wmstoken");
      next("/login");
    }
  }
  if (to.path == "/login" || to.path == "/register") {
    next();
  } else {
    isLogin ? next() : next("/login");
  }
});

export default router;
